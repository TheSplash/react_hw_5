import React from 'react'
import Cards from './Cards'
import '../style/shop.scss'
import { useSelector } from 'react-redux'

const Shop = (/* {toggleFavourites, favorites} */) => {
  const product = useSelector(state => state.products.products)
  const favorites = useSelector(state => state.favorites.favorites)

  return (
    <section className="shop_container">
        {
            product.map((item) => (
                <Cards item={item} key={item.id} 
                boolean={favorites.some((el) => {return el.id === item.id})} 
                />
            ))
        }
    </section>
  )
}

export default Shop

